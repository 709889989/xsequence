package com.xuanner.seq;

import com.xuanner.seq.sequence.Sequence;
import com.xuanner.seq.sequence.impl.SnowflakeSequence;

/**
 * 雪花算法序列号生成器构建
 * Created by xuan on 2018/5/30.
 */
public class SnowflakeSeqBuilder implements SeqBuilder {

    /**
     * 一般可以设置机房的IDC[必选]
     */
    private long datacenterId;
    /**
     * 一般可以设置机器号[必选]
     */
    private long workerId;

    @Override
    public Sequence build() {
        SnowflakeSequence sequence = new SnowflakeSequence();
        sequence.setDatacenterId(this.datacenterId);
        sequence.setWorkerId(this.workerId);
        return sequence;
    }

    public static SnowflakeSeqBuilder create() {
        SnowflakeSeqBuilder builder = new SnowflakeSeqBuilder();
        return builder;
    }

    public SnowflakeSeqBuilder datacenterId(long datacenterId) {
        this.datacenterId = datacenterId;
        return this;
    }

    public SnowflakeSeqBuilder workerId(long workerId) {
        this.workerId = workerId;
        return this;
    }

}
